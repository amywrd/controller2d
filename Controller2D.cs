﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(BoxCollider2D))]
public class Controller2D : MonoBehaviour
{
  struct ColliderCorners
  {
    public Vector2 TopLeft;
    public Vector2 TopRight;
    public Vector2 BottomLeft;
    public Vector2 BottomRight;
  }
  public struct CollisionMatrix
  {
    public bool above;
    public bool below;
    public bool left;
    public bool right;

    public void Reset() { above = below = left = right = false; }
  }

  public LayerMask collisionMask;
  public int horizontalRayCount = 4;
  public int verticalRayCount = 4;
  public CollisionMatrix collisions;

  BoxCollider2D collider;
  ColliderCorners raycastOrigins;

  const float colliderInset = 0.015f;

  float horizontalRaySpacing;
  float verticalRaySpacing;

  void Start()
  {
    collider = GetComponent<BoxCollider2D>();
    CalculateRaySpacing();
  }

  public void Move(Vector3 velocity)
  {
    UpdateRaycastOrigins();
    collisions.Reset();

    if(velocity.x != 0)
      HorizontalCollisions(ref velocity);

    if(velocity.y != 0)
      VerticalCollisions(ref velocity);

    transform.Translate(velocity);
  }
  
  void HorizontalCollisions(ref Vector3 velocity)
  {
    float directionX = Mathf.Sign(velocity.x);
    float rayLength = Mathf.Abs(velocity.x) + colliderInset;

    for (int i = 0; i < horizontalRayCount; i++)
    {
      Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.BottomLeft : raycastOrigins.BottomRight;
      rayOrigin += Vector2.up * (horizontalRaySpacing * i);
      RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

      Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

      if (hit)
      {
        velocity.x = (hit.distance - colliderInset) * directionX;
        rayLength = hit.distance;

        collisions.left = directionX == -1;
        collisions.right = directionX == 1;
      }
    }
  }

  void VerticalCollisions(ref Vector3 velocity)
  {
    float directionY = Mathf.Sign(velocity.y);
    float rayLength = Mathf.Abs(velocity.y) + colliderInset;

    for (int i = 0; i < verticalRayCount; i++)
    {
      Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.BottomLeft : raycastOrigins.TopLeft;
      rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);
      RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

      Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

      if(hit)
      {
        velocity.y = (hit.distance - colliderInset) * directionY;
        rayLength = hit.distance;

        collisions.below = directionY == -1;
        collisions.above = directionY == 1;
      }
    }
  }

  void UpdateRaycastOrigins()
  {
    Bounds bounds = collider.bounds;
    bounds.Expand(colliderInset * -2);

    raycastOrigins.BottomLeft = new Vector2(bounds.min.x, bounds.min.y);
    raycastOrigins.BottomRight = new Vector2(bounds.max.x, bounds.min.y);
    raycastOrigins.TopLeft = new Vector2(bounds.min.x, bounds.max.y);
    raycastOrigins.TopRight = new Vector2(bounds.max.x, bounds.max.y);
  }

  void CalculateRaySpacing()
  {
    Bounds bounds = collider.bounds;
    bounds.Expand(colliderInset * -2);

    horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
    verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);

    horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
    verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
  }



}
